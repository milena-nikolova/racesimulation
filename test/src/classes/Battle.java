package classes;

import java.util.Random;

/**
 * Class Battle - representing a battle.
 *
 * @author Gerry
 *
 */
public class Battle {
	/**
	 * First player.
	 */
	private final Player firstPlayer;
	/**
	 * Second player.
	 */
	private final Player secondPlayer;
	/**
	 * Result of the battle.
	 */
	private final int result;

	/**
	 * Constructor.
	 *
	 * @param firstPlayer
	 *            first player
	 * @param secondPlayer
	 *            second player
	 */
	Battle(Player firstPlayer, Player secondPlayer) {
		this.firstPlayer = firstPlayer;
		this.secondPlayer = secondPlayer;
		this.result = play(this.getFirstPlayer(), this.getSecondPlayer());
	}

	/**
	 * Randomly generates the result of the battle.
	 *
	 * @param firstPlayer
	 *            first player
	 * @param secondPlayer
	 *            second player
	 * @return result
	 */
	private static int play(Player firstPlayer, Player secondPlayer) {
		final Random RANDOM = new Random();
		final int MIN = 0;
		final int MAX = 2;
		final int result;
		if (firstPlayer.equals(secondPlayer)) {
			result = 1;
		} else {
			result = RANDOM.nextInt(MAX - MIN + 1) + MIN;
		}
		if (result == 1) {
			firstPlayer.setPoints(firstPlayer.getPoints() + 1);
		} else if (result == 2) {
			secondPlayer.setPoints(secondPlayer.getPoints() + 1);
		} else {
			firstPlayer.setPoints(firstPlayer.getPoints() + 0.5);
			secondPlayer.setPoints(secondPlayer.getPoints() + 0.5);
		}

		return result;
	}

	/**
	 *
	 * @return first player
	 */
	public Player getFirstPlayer() {
		return firstPlayer;
	}

	/**
	 *
	 * @return second player
	 */
	public Player getSecondPlayer() {
		return secondPlayer;
	}

	/**
	 *
	 * @return result of the battle
	 */
	public int getResult() {
		return result;
	}


	public String resultProperty () {
		switch (result) {
			case 1: return "First Player wins";
			case 2: return "Second Player wins";
			default: return "Nobody wins";
		}
	}
	/**
	 * toString.
	 */
	@Override
	public String toString() {
		String battle = "Player " + this.getFirstPlayer().getId() + " vs. Player " + this.getSecondPlayer().getId();

		String winner;
		if (this.getResult() == 1) {
			winner = " First Player wins";
		} else if (this.getResult() == 2) {
			winner = " Second Player wins";
		} else {
			winner = " Nobody wins";
		}
		return battle + winner + "\n";
	}

}
