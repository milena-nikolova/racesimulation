package classes;


import java.util.ArrayList;
import java.util.List;

/**
 * Class Round - representing a round.
 *
 * @author Gerry
 */
class Round {
    /**
     * List of all the battles that take part in a round.
     */
    private List<Battle> battles;
    /**
     * List of all the available players(those who haven't played in this round
     * yet).
     */
    private List<Player> availablePlayers;
    /**
     * ID of the round.
     */
    private int id;

    /**
     * Constructor.
     *
     * @param id if of the round
     */
    Round(int id) {
        this.id = id;
        this.battles = new ArrayList<>();
        this.availablePlayers = new ArrayList<>();
    }

    /**
     * Adds battle to the list of battles.
     *
     * @param battle battle
     */
    void addBattle(Battle battle) {
        this.battles.add(battle);
    }

    /**
     * @return list of battles
     */
    List<Battle> getBattles() {
        return this.battles;
    }

    /**
     * @return list of the available players
     */
    List<Player> getAvailablePlayers() {
        return this.availablePlayers;
    }

    /**
     * @return id of the round
     */
    /*public int getId() {
        return id;
    }*/

    /**
     * Sets the list of battles.
     *
     * @param battles list of battles.
     */
   /* public void setBattles(List<Battle> battles) {
        this.battles = battles;
    }*/

    /**
     * Sets the list of the available players.
     *
     * @param availablePlayers list of the available players
     */
    void setAvailablePlayers(List<Player> availablePlayers) {
        this.availablePlayers = availablePlayers;
    }

    /**
     * toString.
     */
    @Override
    public String toString() {

        return battles.toString();
    }

}
