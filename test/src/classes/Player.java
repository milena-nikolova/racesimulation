package classes;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Class Player - represents a player.
 *
 * @author Gerry
 */
public class Player implements Comparator<Player> {
    /**
     * ID of the player.
     */
    private int id;
    /**
     * Points of the player.
     */
    private double points;
    /**
     * List of all the players opponents.
     */
    private List<Player> opponents;

    /**
     * Default Constructor.
     */
    Player() {
    }

    /**
     * Constructor.
     *
     * @param id id of the player
     */
    Player(int id) {
        this.id = id;
        this.setPoints(0.0);
        this.opponents = new ArrayList<>();
        this.addOpponent(this);
    }

    /**
     * @return id of the player
     */
    public int getId() {
        return id;
    }

    /**
     * @return points of the player
     */
    public double getPoints() {
        return points;
    }

    /**
     * Setter for player's points.
     *
     * @param points points
     */
    void setPoints(double points) {
        this.points = points;
    }

    List<Player> getOpponents() {
        return opponents;
    }

    /**
     * Adds an opponent to players opponents list.
     *
     * @param opponent opponent
     */
    void addOpponent(Player opponent) {
        this.opponents.add(opponent);
    }

    /**
     * toString.
     */
    @Override
    public String toString() {
        return "id: " + this.getId();
    }

    /**
     * Compares the points of two players.
     */
    @Override
    public int compare(Player pl1, Player pl2) {
        return Double.compare(pl2.getPoints(), pl1.getPoints());

    }
}
