package classes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Class Race - simulates a race.
 *
 * @author Gerry
 */
public final class Race {
    /**
     * Players that will take part in the race.
     */
    private List<Player> players;
    /**
     * Rounds of the race.
     */
    private List<Round> rounds;
    /**
     * Number of players.
     */
    private int numberOfPlayers;
    /**
     * Number of rounds.
     */
    private int numberOfRounds;

    /**
     * Constructor.
     *
     * @param numberOfPlayers number of players
     * @param numberOfRounds  number of rounds
     */
    public Race(int numberOfPlayers, int numberOfRounds) {
        this.numberOfPlayers = numberOfPlayers;
        this.numberOfRounds = numberOfRounds;

        if (numberOfRounds >= numberOfPlayers) {
            throw new InvalidInputException("The number of rounds shouldn't be higher than the number of player");
        }
        this.players = new ArrayList<>(numberOfPlayers);
        addPlayers();
        this.rounds = new ArrayList<>(numberOfRounds);
        this.addRounds();
    }

    /**
     * Adds all existing players in the list of players.
     */
    private void addPlayers() {
        for (int i = 0; i < numberOfPlayers; i++) {
            players.add(new Player(i));

        }
    }

    /**
     * Adds all rounds and all battles in each round.
     */
    private void addRounds() {
        for (int i = 0; i < numberOfRounds; i++) {
            Round round = new Round(i);
            List<Player> availablePlayers = new ArrayList<>(players);
            round.setAvailablePlayers(availablePlayers);
            rounds.add(round);
            addBattles(round);
            Collections.sort(players, new Player());
        }
    }

    /**
     * Simulates all the battles in one round.
     *
     * @param round round
     */
    private void addBattles(Round round) {

        for (int i = 0; i < numberOfPlayers / 2; i++) {

            Player firstPlayer = findAvailablePlayer(i, round);

            Player secondPlayer = players.get((numberOfPlayers / 2) + i);
            if (firstPlayer.getOpponents().contains(secondPlayer) || firstPlayer.equals(secondPlayer)) {
                secondPlayer = pickOpponent(firstPlayer, round);
            }

            firstPlayer.addOpponent(secondPlayer);
            secondPlayer.addOpponent(firstPlayer);

            round.getAvailablePlayers().remove(firstPlayer);
            round.getAvailablePlayers().remove(secondPlayer);
            round.addBattle(new Battle(firstPlayer, secondPlayer));
        }
        if (numberOfPlayers % 2 != 0) {
            Player pl1 = round.getAvailablePlayers().get(0);
            round.addBattle(new Battle(pl1, pl1));
        }

    }

    /**
     * Finds available player for a specific round.
     *
     * @param index index of the list of players
     * @param round current round
     * @return available player
     */
    private Player findAvailablePlayer(int index, Round round) {
        Player player = players.get(index);
        while (!round.getAvailablePlayers().contains(player)) {
            index++;
            player = players.get(index);
        }
        return player;
    }

    /**
     * Picking an opponent for a specific player. The opponent has to be one of
     * the available players. Also the opponent has to be someone, who is not a
     * part of the player's opponents list.
     *
     * @param firstPlayer the player in need of opponent
     * @param round       current round
     * @return the chosen opponent
     */
    private Player pickOpponent(Player firstPlayer, Round round) {
        final Random RANDOM = new Random();
        final int MIN = 0;
        final int MAX = round.getAvailablePlayers().size() - 1;

        int index = RANDOM.nextInt(MAX - MIN + 1) + MIN;

        Player secondPlayer = round.getAvailablePlayers().get(index);

        while (firstPlayer.getOpponents().contains(secondPlayer)) {

            index = RANDOM.nextInt(MAX - MIN + 1) + MIN;
            secondPlayer = round.getAvailablePlayers().get(index);
        }

        return secondPlayer;
    }

    /**
     * @return list of players
     */
    public List<Player> getPlayers() {
        return this.players;
    }

    /**
     * @return list of rounds
     */
    private List<Round> getRounds() {
        return this.rounds;
    }

    /**
     * toString.
     */
    @Override
    public String toString() {
        return this.rounds.toString();
    }

    public List<Battle> getBattlesForPlayer(int playerId) {
        List<Battle> outputBattleList = new ArrayList<>();
        this.getRounds().forEach(round -> {
            round.getBattles().forEach(battle -> {
                if (battle.getFirstPlayer().getId() == playerId || battle.getSecondPlayer().getId() == playerId) {
                    outputBattleList.add(battle);
                }
            });
        });
        return outputBattleList;
    }

}
