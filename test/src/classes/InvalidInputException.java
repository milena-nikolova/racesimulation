package classes;

/**
 * Invalid input exception.
 * @author Gerry
 *
 */
public final class InvalidInputException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Method that shows the message to be printed.
	 * 
	 * @param message
	 *            message to be printed
	 */
	InvalidInputException(String message) {
		super(message);
	}

}