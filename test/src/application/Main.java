package application;

import classes.Battle;
import classes.InvalidInputException;
import classes.Player;
import classes.Race;
import javafx.application.Application;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.List;

public class Main extends Application {
    private static Race raceData;
    private static ObservableList<Player> players = FXCollections.observableArrayList();
    private Stage primaryStage;

    /**
     * @param primaryStage primary stage of the app
     */
    @Override
    public void start(Stage primaryStage) {
        try {
            primaryStage.setTitle("Racing Simulation");

            GridPane grid = new GridPane();
            grid.setAlignment(Pos.CENTER);
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(25, 25, 25, 25));

            Scene scene = new Scene(grid, 300, 275);
            primaryStage.setScene(scene);

            Text sceneTitle = new Text("Enter your data");
            sceneTitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
            grid.add(sceneTitle, 0, 0, 2, 1);

            Label lblNumberOfPlayers = new Label("Players:");
            grid.add(lblNumberOfPlayers, 0, 1);

            NumberTextField tfNumberOfPlayers = new NumberTextField();
            grid.add(tfNumberOfPlayers, 1, 1);

            Label pw = new Label("Rounds:");
            grid.add(pw, 0, 2);

            NumberTextField tfNumberOfRounds = new NumberTextField();
            grid.add(tfNumberOfRounds, 1, 2);

            Button btn = new Button("Simulate");
            btn.setOnAction((e) -> {
                try {
                    raceData = new Race(Integer.parseInt(tfNumberOfPlayers.getText()),
                            Integer.parseInt(tfNumberOfRounds.getText()));
                    tablePlayerInfo();
                } catch (InvalidInputException ex) {
                    Alert dialog = new Alert(Alert.AlertType.ERROR);
                    dialog.setHeaderText("Invalid Input");
                    dialog.setContentText("The players must be more than the rounds");
                    dialog.showAndWait();
                }
            });

            HBox hbBtn = new HBox(10);
            hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
            hbBtn.getChildren().add(btn);

            grid.add(hbBtn, 1, 4);

            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Lists all players sorted by points.
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    private void tablePlayerInfo() {
        if (!raceData.getPlayers().isEmpty()) {
            final Stage raceInfo = new Stage();
            players = FXCollections.observableArrayList(raceData.getPlayers());

            raceInfo.initModality(Modality.APPLICATION_MODAL);
            raceInfo.initOwner(primaryStage);
            raceInfo.setTitle("Result");

            TableView<Player> raceInfoTable = new TableView<>();

            TableColumn<Player, Integer> playerId = new TableColumn<>("ID");
            playerId.setCellValueFactory(new PropertyValueFactory("id"));
            playerId.setPrefWidth(200);

            TableColumn<Player, Double> playerPoints = new TableColumn<>("Points");
            playerPoints.setCellValueFactory(new PropertyValueFactory("points"));
            playerPoints.setPrefWidth(200);

            raceInfoTable.getColumns().setAll(playerId, playerPoints);
            raceInfoTable.setItems(players);

            final VBox vbox = new VBox();
            vbox.setSpacing(5);
            vbox.setAlignment(Pos.CENTER);
            vbox.setPadding(new Insets(10, 10, 10, 10));
            vbox.getChildren().addAll(raceInfoTable);

            /**
             * Click event handler for row data. Lists all battles for selected player.
             */
            raceInfoTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
                if (newSelection != null) {
                    tablePlayerData(raceData.getBattlesForPlayer(newSelection.getId()));
                }
            });

            Scene informationScene = new Scene(vbox);
            raceInfo.setScene(informationScene);
            raceInfo.show();
        }
    }

    /**
     * Creates table with each battle for the selected player.
     * @param playerData information about all battler for current player
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    private void tablePlayerData(List<Battle> playerData) {
        if (!raceData.getPlayers().isEmpty()) {
            final Stage playerInfo = new Stage();
            players = FXCollections.observableArrayList(raceData.getPlayers());

            playerInfo.initModality(Modality.APPLICATION_MODAL);
            playerInfo.initOwner(primaryStage);
            playerInfo.setTitle("Battles for selected player");

            TableView<Battle> playerInfoTable = new TableView<>();

            TableColumn<Battle, Player> firstPlayer = new TableColumn<>("Player 1");
            firstPlayer.setCellValueFactory(new PropertyValueFactory("firstPlayer"));
            firstPlayer.setPrefWidth(200);

            TableColumn<Battle, Player> secondPlayer = new TableColumn<>("Player 2");
            secondPlayer.setCellValueFactory(new PropertyValueFactory("secondPlayer"));
            secondPlayer.setPrefWidth(200);

            TableColumn<Battle, String> result = new TableColumn<>("Winner");
            result.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().resultProperty()));
            result.setPrefWidth(200);

            playerInfoTable.getColumns().setAll(firstPlayer, secondPlayer, result);
            playerInfoTable.setItems(FXCollections.observableArrayList(playerData));

            final VBox vbox = new VBox();
            vbox.setSpacing(5);
            vbox.setAlignment(Pos.CENTER);
            vbox.setPadding(new Insets(10, 10, 10, 10));
            vbox.getChildren().addAll(playerInfoTable);

            Scene informationScene = new Scene(vbox);
            playerInfo.setScene(informationScene);
            playerInfo.show();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
